package model;

import java.time.LocalDate;

public class WeatherInformation {
    private int weatherInfoId;
    private double temperature;
    private int pressure;
    private int humidity;
    private LocalDate weather_date;
    private Wind wind;

    public WeatherInformation() {}

    public WeatherInformation(int weatherInfoId, double temperature, int pressure, LocalDate weather_date) {
        this.weatherInfoId = weatherInfoId;
        this.temperature = temperature;
        this.pressure = pressure;
        this.weather_date = weather_date;
    }

    public WeatherInformation(int weatherInfoId, double temperature, int pressure, int humidity, LocalDate weather_date, Wind wind) {
        this.weatherInfoId = weatherInfoId;
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.weather_date = weather_date;
        this.wind = wind;
    }
}
