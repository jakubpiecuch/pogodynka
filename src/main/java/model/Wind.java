package model;

public class Wind {
    private int windId;
    private double speed;
    private double degree;

    public Wind(int windIdl) {
        this.windId = windIdl;
    }

    public Wind(int windIdl, double speed, double degree) {
        this.windId = windIdl;
        this.speed = speed;
        this.degree = degree;
    }

}
