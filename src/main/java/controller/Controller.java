package controller;

import java.time.LocalDate;

public abstract class Controller {
    private boolean running = true;
    private final String MENU_OPTIONS = """
            Choose an option:
                    1. Add Location
                    2. Show available locations
                    3. Show weather
                    4. Exit application
            """;

    public Controller(String s) {
        do {
            initMenu(s);
        } while (running);
    }

    private void initMenu(String s){

        showMessage(MENU_OPTIONS);
        int input = Integer.parseInt(s);
        executeOption(input);
    };

    private void executeOption(int input){
        switch(input){
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                end();
                break;
        }
    }

    private void end(){
        running = false;
    }
    public abstract void showMessage(String s);
    public abstract String readString(String s);
    public abstract int readInt(String s);
    public abstract LocalDate readDate(String s);
}
