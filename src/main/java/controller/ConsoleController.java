package controller;

import java.time.LocalDate;

public class ConsoleController extends Controller{

    public ConsoleController(String s) {
        super(s);
    }

    public void showMessage(String s){
        System.out.println(s);
    }

    @Override
    public String readString(String s) {
        return s;
    }

    @Override
    public int readInt(String s) {
        return Integer.parseInt(s);
    }

    @Override
    public LocalDate readDate(String s) {
        return LocalDate.parse(s);
    }
}
