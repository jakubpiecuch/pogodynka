import java.util.Scanner;

public class WeatherApp {
    public static void main(String[] args) {
        boolean running = true;
        int menuOption = 0;
        String option = "";

        while (running) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("""
                    Choose an option:
                    1. Add Location
                    2. Show available locations
                    3. Show weather
                    4. Exit application
                    """);
            menuOption = scanner.nextInt();
            switch (menuOption) {
                case 1:
                    //addLocation();
                    break;
                case 2:
                    //showAllLocations();
                    break;
                case 3:
                    System.out.println("Do you want to specify particular date? (y/n)");
                    scanner.nextLine();
                    option = scanner.nextLine();
                    if (option.equalsIgnoreCase("y")) {
                        System.out.println("Date (YYYY-MM-DD): ");
                        System.out.println(scanner.nextLine());
                    } else if (option.equalsIgnoreCase("n")) {
                        System.out.println("Weather will be shown for tomorrow.\n");
                    } else {
                        System.out.println("Invalid input! ");
                    }

                    System.out.println("Do you want to specify particular location? (y/n)");
                    option = scanner.next();
                    if (option.equalsIgnoreCase("y")) {
                        System.out.println("Location: ");
                        System.out.println(scanner.nextLine());
                    } else if (option.equalsIgnoreCase("n")) {
                        System.out.println("Weather will be shown for Warsaw\n");
                    } else {
                        System.out.println("Invalid input!");
                    }
                    break;
                case 4:
                    //end();
                    running = false;
                    break;
            }
        }
    }
}
